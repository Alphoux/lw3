#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>

volatile long int overflow = 0;
volatile long int overflow2 = 0;

ISR(TIMER0_OVF_vect){
	overflow ++;
}

ISR(TIMER1_COMPA_vect){
	overflow2++;
}

void Timer0_init(){
    TCCR0B |= (1<<CS02);
  	TCNT0 = 0;
   	TIMSK0 |= (1<<TOIE0);
}

void Timer1_init(){
  	TCCR1A |= (1<<WGM11)|(1<<WGM10);
    TCCR1B |= (1<<CS10)|(1<<CS12)|(1<<WGM12)|(1<<WGM13);
    TCNT1 = 0;
    OCR1A = 144;
    TIMSK1 |= (1<<OCIE1A);
}

int main(void){
  DDRC = 0xFF;
  DDRB = 0b11111100;
  DDRD = 0;
  PORTD = 0x00;
  PORTB = 0x00;
  PORTC = 0;
  Timer0_init();
  Timer1_init();
  sei();
  //mandatory_part();
  additionnal_part();
}

int mandatory_part(void){
  	while(1){
      if(overflow >= 244){
      	overflow = 0;
        PORTB ^= (1<<PB2);
        TCNT0 = 0;
      }
    }
}
int additionnal_part(){
	while(1){
      if(overflow2 >= 122){
      	overflow2 = 0;
        PORTB ^= (1<<PB3);
        TCNT1 = 0;
      }
    }
}